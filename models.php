<?php

class ADDRESS_MODEL  extends EAV_MODEL {
		public $name = __CLASS__;
		public $entity_table_name = 'address_entity';
		public $entity_table_pk = 'id';
		public $entity_table_additional = array('created','user_id');

		public $link_table_name = 'address_data';
		public $link_table_entity = 'entity_id';
		public $link_table_attribute = 'attribute_id';
		public $link_table_value = 'value_id';
		public $link_table_additional = array();

        public $attribute_table_name = 'address_attribute';
		public $attribute_table_pk = 'id';
		public $attribute_table_value = 'attribute';
		public $attribute_table_type     = false;

        public $value_table_name         = 'address_value';
        public $value_table_pk           = 'id';
        public $value_table_value        = 'value';
			
	}