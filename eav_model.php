<?php
include("eav_entity.php");
	

	class EAV_MODEL{
        var $cache_attributes = true;
        var $cache_values = true;

        var $store_values = array();
        var $store_attributes = array();// cache for attribute id's
        var $debug =array();

		//returns array(true or errormessage, insert_id)  (for good use look at php.net/list)
		function store($pk, $input_data){
			//insert data into the entity table
			$insert_data = array();
			foreach($this->entity_table_additional as $name){
				if(isset($input_data[$name])){
					$insert_data[$name] = (string)$input_data[$name]['value'];
                    unset($input_data[$name]);
				}
			}
            if($pk == null){ // the record has to be inserted as new
                $add = fastInsert($this->entity_table_name, $insert_data);
                if($add == false){
                    return array("Error while inserting entity",0);             
                }
            }else{ // update the record
                $where = array($this->entity_table_pk => $pk);
                $q =  fastSelect($this->entity_table_name,$where);
                if(count($q) > 0){
                    fastEdit($this->entity_table_name,$insert_data,$where);   
                    $add = $pk;                 
                }else{
                    throw new Exception("Record does not excist".$pk);
                }                
            }
            
            //get the id's of all the used attributes
            $keys = $this->getAttributeIds(array_keys($input_data),true);

            //get the id's of all the used values
            $values = array();
            foreach ($input_data as $key => $value) {
               $values[] = $value['value'];
            }
            $values = $this->getValueIds($values,true);


            $inserts = array();//list of all the insertable data
            foreach($input_data as $id => $value){
                if(!isset($values[$value['value']])){
                    throw new Exception("Attribute not found!".$id);
                }else{
                    $attribute = $keys[$id];
                }
                
                if(!isset($values[$value['value']])){
                    throw new Exception("Value not found!".$value['value']);
                }else{
                    $data = $values[$value['value']];
                }
                // insert into data table
                if($pk == null || $value['state'] == 'insert'){
                    $inserts[] = "(".$add.",".$attribute.",".$data.")";
                }elseif($value['state'] == 'update'){
                    $attr= array($this->link_table_value=>$values[$value['value']]);
                    $where = array($this->link_table_entity=>$add, $this->link_table_attribute=>$attribute);
                    fastEdit($this->link_table_name,$attr,$where);
                }elseif($value['state'] == 'delete'){
                    $where = array($this->link_table_entity=>$add, $this->link_table_attribute=>$attribute);
                    fastDelete($this->link_table_name,$where);
                }

               
            }
            if(count($inserts) != 0){// are there inserts (the inserts are stacked for increased preformance)
                $q = "INSERT INTO ".$this->link_table_name." (".$this->link_table_entity.",". $this->link_table_attribute.",".$this->link_table_value.") VALUES";
                $q .= implode(",", $inserts);
                if(queryToArray($q)){

                    throw new Exception("An error has occured while trying to insert the link data"); 
                }
            }
            if($pk == null){
                $this->debug("Inserted new record to entityid ".$add);
            }else{
                $this->debug("Edited record with entityid ".$add);
            }
            
            return array(true,$add);  
		}

        //returns true or errormessage
        function remove($id){

            fastDelete($this->entity_table_name,array($this->entity_table_pk=>$id));
            fastDelete($this->link_table_name,array($this->link_table_entity=>$id));
        }

        // create an new Entity (don't forget to store it afterwards)
        function create(){
            return $this->get();
        }

        //internal function for checking if it are numbers
        function number_check($inp){
                $inp = (string)$inp;
                if(ctype_digit($inp)){
                    return $inp;
                }else{
                    throw new Exception("One of the values is not an number: ".htmlspecialchars(print_r($inp)));
                }
            }

		// returns array(name=>'', etc)  or false
		function get($inp_pk=null){

            if($inp_pk == null){//create an empty EAV object (no pk creates an new entry)
                 return new EAV_ENTITY($this,null, array());
            }elseif(!is_array($inp_pk)){//if its not an array make it one (usability thingy)
                $inp_pk = array($inp_pk);
            }
            
            $inp_pk =  array_map(array($this,"number_check"), $inp_pk);
            if(count($inp_pk) == 0){
                throw new Exception("pk in ->get(pk) is not properly set either get(array(1,2,3)) or get(1)");
            }

            $entities = queryToArray("Select * from `".$this->entity_table_name."` WHERE `".$this->entity_table_pk."` IN (".implode(",",$inp_pk).")");
            
            if(count($entities) > 0){ // are there entries found?
                $output = array();
                foreach($entities as $insid =>$insert_data){

                    //save pk but dont sent it in the data
                    $pk = $insert_data[$this->entity_table_pk];
                    unset($insert_data[$this->entity_table_pk]);

                    // Get the in table data
                    foreach( $insert_data as $id => $value){
                        $output[$pk][$id]['intable'] = true;
                        $output[$pk][$id]['state'] = '';
                        $output[$pk][$id]['value'] = $value;
                    }
                    
                    //select the link tables with data
                    $sql = "select a.".$this->attribute_table_value." as attribute, v.".$this->value_table_value." as value
                            from ".$this->link_table_name." as l
                            LEFT JOIN ".$this->value_table_name." as v ON v.".$this->value_table_pk." = l.".$this->link_table_value."
                            LEFT JOIN ".$this->attribute_table_name." as a ON a.".$this->attribute_table_pk." = l.".$this->link_table_attribute."
                            Where l.".$this->link_table_entity." = '".addslashes($pk)."' ";
                            
                    $query = queryToArray($sql);
                    foreach($query as $att){
                        if(!isset($output[$att['attribute']])){
                            $output[$pk][$att['attribute']]['value'] = $att['value'];
                            $output[$pk][$att['attribute']]['intable'] = false;    
                            $output[$pk][$att['attribute']]['state'] = '';                    
                        }
                    }

                }
                //check if the input keys equal the output keys
                if(count($output) == count($inp_pk)){
                    if(count($output) == 1){
                        foreach($output as $n => $v){
                            return new EAV_ENTITY($this,$n, $v);
                            
                        }
                    }else{
                        $ret = array();
                        echo count($entities);
                        foreach($output as $n => $v){
                            $ret[] = new EAV_ENTITY($this,$n, $v);
                            
                        }
                        return $ret;
                    }
                }else{
                    throw new Exception("input and output are not the same ".count($output)."!=".count($pk));
                }
            
            }else{
                $this->debug("No entries found for ",$inp_pk);
                return false;
            }
		}
		

		// returns array(1,2,3,4,5,6)
		function query($array){

            //get the keys and values from the tables
            $keys = $this->getAttributeIds(array_keys($array));
            $values = $this->getValueIds($array);           
            $possible = null;
            foreach($array as $k => $v){
                if(isset($keys[$k])){ // if the "attribute" is not in the database
                    if( isset($values[$v])){ // there is no excepition when you dont have a value 
                        $where = array($this->link_table_attribute => $keys[$k],
                        $this->link_table_value => $values[$v]);
                        $result = fastSelect($this->link_table_name,$where);
                        $out = array();
                        foreach($result as $r){
                            $out[] = $r[$this->link_table_entity];
                        }
                        // you can not intersect the first time if there is 
                        //   nothing to intersect (it will stay empty)
                        // intersecting is far faster than leftjoining
                        // (there is probably a tippingpoint if you use alot filter statements)
                        if($possible == null){
                            $possible = $out;
                        }else{                            
                            $possible = array_intersect($out, $possible);
                        }
                    }else{
                        //if there is no match with a value no record can match this query
                        //and there is no need to look further
                        $this->debug("There is no value in the database for ".$v, $array);
                        return array();
                    }

                }else{
                    throw new Exception("Key  has not not been found in the database");
                }
            }

            if($possible == null){//none of all the query parts have matched
                $this->debug("There are no results found for value ", $array);
                return array();
            }else{
                return $possible;
            }
		}

        //this function saves the debug messages
        function debug($msg,$settings = null){
            $html = "";
            $backtr = debug_backtrace();
            $html .= "DEBUG: line: ".$backtr[0]['line']." file: ".$backtr[0]['file']."\n";
            $html .= "--MSG: <b>".$msg."</b>\n";
            if($settings != null){
                $html .= "DATA:".print_r($settings,true)."\n";                
            }
            $this->debug[] = $html;
        }
        //this func prints the debug info
        function getDebug(){
            $html  = "<pre>DEBUG SYSTEM EAV_MODEL (".count($this->debug)." messages)\n";
            $html .= implode("",$this->debug);
            $html .= "</pre>";
            return $html;
        }
        
        // returns array(1,2,3,4,5,6)
        function getAttributeIds($array){
            $array =  array_map("addslashes", $array);
            $output = array();
            foreach($array as $name){
                if(isset($this->store_attributes[$name])){
                    $output[$name] = $this->store_attributes[$name];
                    unset($array[$name]);
                }
            }

            $data = implode("', '", $array);           
            $query = queryToArray('select *
                    from '.$this->attribute_table_name."
                    where ".$this->attribute_table_value ." in ('".$data."')");
            
            foreach ($query as $value) {
                $output[$value[$this->attribute_table_value]] = $value[$this->attribute_table_pk];
                if($this->cache_attributes == true){
                    $store_attributes[$value[$this->attribute_table_value]] = $value[$this->attribute_table_pk];                    
                }
            }
            $missing = array_diff($array,array_keys($output) );
            if(count($missing)> 0){
                fastMultiInsert($this->attribute_table_name,array($this->attribute_table_value),$missing);
                $output = array_merge($output, $this->getAttributeIds($missing));
                $this->debug("Creating new attributes in the database",$missing);
            }

            return $output;
        }
        
        // returns array(1,2,3,4,5,6)
        function getValueIds($array){
            $array =  array_map("addslashes", $array);
            $output = array();
            foreach($array as $name){
                if(isset($this->store_values[$name])){
                    $output[$name] = $this->store_values[$name];
                    unset($array[$name]);
                }
            }
            $data = implode("', '", $array);           
            $query = queryToArray('select *
                    from '.$this->value_table_name."
                    where ".$this->value_table_value ." in ('".$data."')");
            $output = array();
            foreach ($query as $value) {
                $output[$value[$this->value_table_value]] = $value[$this->value_table_pk];
                if($this->cache_values === true){
                    $store_values[$value[$this->value_table_value]]= $value[$this->value_table_pk];                    
                }
            }
            $missing = array_diff($array,array_keys($output) );

            if(count($missing)> 0){
                fastMultiInsert($this->value_table_name,array($this->value_table_value),$missing);
                $output = array_merge($output, $this->getValueIds($missing));  
                $this->debug("Creating new values in the database",$missing);              
            }
            
            return $output;
        }




	}




function fastEdit($table, $variables, $where) {
    $result_names = array();
    $where_names = array();
    foreach($variables as $name => $value) {
        $result_names[] = " `".$name."` = '".mysql_real_escape_string((string)$value)."' ";
    }
    foreach($where as $name => $value) {
        $where_names[] = " `".$name."` = '".mysql_real_escape_string((string)$value)."' ";
    }

    $sql = "UPDATE `".$table."` SET";
    $sql .= "".implode(",", $result_names)."";
    $sql .= "WHERE";
    $sql .= "(".implode(" AND ", $where_names).")";

    queryToArray($sql);

}
function fastInsert($table, $variables) {
    $result_names = array();
    $result_value = array();
        foreach($variables as $name => $value) {
            $result_names[] = "`".(string)$name."`";
            $result_value[] = "'".mysql_real_escape_string((string)$value)."'";
        }

    $sql = "INSERT INTO `".$table."` ";
    $sql .= "(".implode(",", $result_names).")";
    $sql .= "VALUES";
    $sql .= "(".implode(",", $result_value).")";
    queryToArray($sql);
    return mysql_insert_id();

}

function fastMultiInsert($table,$pattern, $variables) {
    $result_names = array();
    $single = count($pattern) == 1 ? true:false;
        foreach($pattern as $name ) {
            $result_names[] = "`".(string)$name."`";
        }
         $sql = "INSERT INTO `".$table."` ";
        $sql .= "(".implode(",", $result_names).")";
        $sql .= "VALUES";
        $queries = array();
        foreach($variables  as $value) {
            $d = array();
            if($single === false){
                 foreach($value  as $value2){
                    $d[] = "'".mysql_real_escape_string((string)$value2)."'";
                     $queries[] = "(".implode(",", $d).")";
                 }
            }else{
                $queries[] = "('".mysql_real_escape_string((string)$value)."')";
            }
            
           
           
            // if(count($queries) > 1000){
            //     queryToArray($sql.implode(",",$queries));
            //     $queries = array();
            // }
        }  
         
if(count($queries) > 0){
        queryToArray($sql.implode(",",$queries));
    
}

   
    
    return true;

}

function fastDelete($table, $where = array()) {

    $where_names = array();
    foreach($where as $name => $value) {
        if (is_array($value)) {
            $where_names[] = " `".$name."` ".$value[0]." ".$value[1]." ";
        } else {
            $where_names[] = " `".$name."` = '".mysql_real_escape_string($value)."' ";
        }
    }

    $sql = "DELETE from `".$table."`";
    if (count($where_names) > 0) {
        $sql .= " WHERE ".implode(" AND ", $where_names)."";
    }
    return queryToArray($sql);

}


function fastSelect($table,$where = array(),$orderby = null,$direction = "asc"){
    
        $where_names = array();     
        foreach($where as $name => $value){
            if(is_array($value)){
                $where_names[] = " `".$name."` ".$value[0]." ".$value[1]." ";
            }else{
            $where_names[] = " `".$name."` = '".mysql_real_escape_string($value)."' ";
            }
        }

        $sql = "SELECT * from `".$table."`";
        if(count($where_names) > 0){
        $sql .= " WHERE ".implode(" AND ",$where_names)."";
        }
        if($orderby != null){
            $sql .= "Order by ".$orderby." ".$direction;
        }
        return queryToArray($sql);

}

function queryToArray($sql) {
    if ($query = mysql_query($sql)) {
        $result = array();
        if (@mysql_num_rows($query) > 0) {
            while ($row = mysql_fetch_assoc($query)) {
                foreach($row as $id => $val) {
                    $row[$id] = stripslashes($val);
                }
                $result[] = $row;
            }

        }
        return $result;
    } else {
        die("Error ".mysql_error());
    }

}