<?php

class EAV_ENTITY{

	private $eav_model = null;
	private $_pk = null;
	private $data = array();

	function setAttribute($name,$value){
		if(isset($this->data[$name]) && $value === null){
			$this->data[$name]['state'] = 'delete';
			$this->data[$name]['value'] = null;
		}elseif(isset($this->data[$name]) ){
			$this->data[$name]['state'] = 'update';
			$this->data[$name]['value'] = sprintf("%s",$value);
		}else{
			//insert
			$this->data[$name]['state'] = 'insert';
			$this->data[$name]['value'] = sprintf("%s",$value);
			return true;
		}
	}
	function getAttribute($name,$notset=null){
		if(isset($this->data[$name]) ){
			return $this->data[$name];
		}
		return $notset;
	}

	function __construct($eav_model,$_pk, $data){
		$this->eav_model = $eav_model;		
		$this->_pk = $_pk;		
		$this->data = $data;		

	}

	function store(){
		list($err,$id) = $this->eav_model->store($this->_pk, $this->data);
		if($err == true){
			$this->_pk = $id;
			foreach($this->data as $name => $data){
				$this->data[$name]['state'] = '';
			}
		}
		return array($err,$id);
	}

	function getArray(){
		$output = array();
		foreach($this->data as $name => $data){
			$output[$name] = $data['value'];
		}
		return $output;
	}
	//using copy because clone is reseved word
	function copy(){
		$new = new EAV_ENTITY($this->eav_model,null, array());
		foreach($this->data as $name => $value){
			$new->setAttribute($name,$value['value']);
		}
		return $new;
	}

	function __get($name){
		return $this->getAttribute($name);
	}

	function __set($name,$value){
		return $this->setAttribute($name, $value);
	}

	function __toString(){
		$html = "<pre style='border:1px solid #DDD'>";
		$backtr = debug_backtrace();
		$html .= "DEBUG: line: ".$backtr[0]['line']." file: ".$backtr[0]['file']."\n";
		$pk = $this->_pk == null ? '{First needs to be stored}' :print_r($this->_pk,true);
		$html .= "unique id: ".$pk."\n";
		if(count($this->data) > 5){
			$debstr = "debugtostr".microtime(true);
			$html .="<a href='Javascript:var e = document.getElementById(\"".$debstr."\");e.style.display = e.style.display ==\"\" ? \"none\" :\"\";'>Show/Hide Data</a>
			<div style='display:none' id='".$debstr."'>";
		}
		foreach($this->data as $name => $data){
			$st = $data['state'] != ''? "(".$data['state'].")" : '';
			$html .= "<b>".$name.str_repeat(" ", 10- strlen($name)). "</b> => ". $data['value'].$st."\n";
		}
		if(count($this->data) > 5){
			$html .= "</div>";
		}
		$html .= "</pre>";
		return $html;
	}

}